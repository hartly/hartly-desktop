// TODO: Load sidebar width from config
document.querySelector('#sidebar').style.width = '300px';

const app = createApp({
    data() {
        // TODO: Load chats from main process
        return {
            currentChatIndex: -1,
            chats: [
                { id: 1, title: 'My journey with Vue' },
                { id: 2, title: 'Blogging with Vue' },
                { id: 3, title: 'Why Vue is so fun' }
            ]
        }
    },
    computed: {
        currentSidebarComponent() {
            // TODO rewrite this function
            return 'chat-list';
        },
        currentViewComponent() {
            // TODO rewrite this function
            // this.currentChatIndex < 0 ? '' :
            return 'chat-view';
        }
    },
    methods: {
        updateChatList(value) {
            this.chats = value;
        },
        switchChat(i) {
            this.currentChatIndex = i;
            ipcRenderer.send('switchChat', i);
        },
        getCurrentChat() {
            return this.chats[this.currentChatIndex] || 'hi';
        }
    }
    // mounted() {
    //     ipcRenderer.invoke('switchChat').then((versions) => {
    //         this.versions = versions;
    //     });
    // }
});

app.component('chat-list', {
    props: ['chats'],
    data() {
        return {
            currentIndex: -1
        }
    },
    methods: {
        switchChat(i) {
            this.currentIndex = i;
            ipcRenderer.send('switchChat', i);
        }
    },
    template: `
        <ul class="chat-list vsk-list vsk-list--two-line vsk-list--avatar-list">
            <li class="vsk-list-item"
                v-for="(chat, index) in chats" :class="{'vsk-list-item--activated': index == currentIndex}"
                @click="switchChat(index)">
                <span class="vsk-list-item__graphic" style="font-size: 40px;">
                    <svg height="48px" width="48px" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                </span>

                <span class="vsk-list-item__text">
                    <span class="vsk-list-item__primary-text">{{ chat.title }}</span>
                    <span class="vsk-list-item__secondary-text">{{ chat.id }}</span>
                </span>
            </li>
        </ul>`
});

app.component('chat-view', {
    props: ['chat'],
    template: `<div class="demo-tab"><a>{{ chat.title }}</a> component</div>`
});

const appMount = app.mount('#root');
