const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const server = require('./server')('127.0.0.1', 5555);
console.log(server);

function createWindow() {
    const mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        transparent: true,
        frame: false,
        webPreferences: {
            devTools: (process.env.NODE_ENV !== 'production'),
            contextIsolation: false,
            nodeIntegration: false,
            preload: path.join(__dirname, 'preload.js')
        }
    });

    mainWindow.loadFile(path.join(__dirname, 'static/index.html')).then(() => server.connect());
}

app.whenReady().then(() => {
    // Delay need to use transparent tag for window
    setTimeout(() => { createWindow() }, 500);

    app.on('activate', function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        // TODO: Check for delay necessity (see 23 line)
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});
