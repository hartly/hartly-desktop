const electron = require('electron');
const vue = require('vue');

process.once('loaded', () => {
    // Provide some libraries and functions to render process
    global.ipcRenderer = electron.ipcRenderer;
    global.createApp = vue.createApp;
});
