const net = require('net');
const sodium = require('libsodium-wrappers');

let _host, _port;
let socket;

const connect = async (host, port, serverKey) => {
    socket = net.createConnection(port, host);

    socket.on('close', (hadError) => {
        console.log('close', hadError);
        // TODO: Reconnect
    });

    socket.on('connect', () => {
        console.log('connect');
    });

    socket.on('data', (data) => {
        console.log('data', data);
    });

    socket.on('drain', () => {
        console.log('drain');
    });

    socket.on('end', () => {
        console.log('end');
    });

    socket.on('error', (err) => {
        console.log('error', err);

    });

    socket.on('lookup', (err, address, family, host) => {
        console.log('lookup', err, address, family, host);
    });

    socket.on('ready', async() => {
        console.log('ready');
        // TODO: Send handshake
        await sodium.ready;

        let l = new Uint8Array(serverKey.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
        console.log(l.length);
        console.log(l);
        let enc = sodium.crypto_box_seal('handshake', l);
        send(enc);
    });

    socket.on('timeout', () => {
        console.log('timeout');
    });

    return Promise
};

const send = data => {
    if (data.length > 65535)
        throw new Error();

    let _data = Array.from(data);

    _data.unshift((_data.length / 256).toFixed(), _data.length % 256);

    let buf = Uint8Array.from(_data);

    console.log(buf.unshift);
    console.log(buf);
    socket.write(buf);
};

module.exports = (host, port) => {
    _host = host;
    _port = port;

    return { connect, send };
};
